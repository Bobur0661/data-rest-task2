package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.entity.Profession;
import uz.pdp.task2.projection.BrandProjection;
import uz.pdp.task2.projection.ProfessionProjection;

@RepositoryRestResource(path = "profession", collectionResourceRel = "list", excerptProjection = ProfessionProjection.class)
public interface ProfessionRepo extends JpaRepository<Profession, Long> {


}
