package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.entity.Blog;
import uz.pdp.task2.projection.AddressProjection;
import uz.pdp.task2.projection.BlogProjection;

@RepositoryRestResource(path = "blog", collectionResourceRel = "list", excerptProjection = BlogProjection.class)
public interface BlogRepo extends JpaRepository<Blog, Long> {


}
