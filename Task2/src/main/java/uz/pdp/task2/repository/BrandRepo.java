package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.projection.AddressProjection;
import uz.pdp.task2.projection.BrandProjection;

@RepositoryRestResource(path = "brand", collectionResourceRel = "list", excerptProjection = BrandProjection.class)
public interface BrandRepo extends JpaRepository<Brand, Long> {


}
