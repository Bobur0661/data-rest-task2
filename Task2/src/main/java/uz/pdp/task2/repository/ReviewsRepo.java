package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.Reviews;

public interface ReviewsRepo extends JpaRepository<Reviews, Long> {


}
