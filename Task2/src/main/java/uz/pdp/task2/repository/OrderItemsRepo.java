package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.OrderItems;

public interface OrderItemsRepo extends JpaRepository<OrderItems, Long> {

}
