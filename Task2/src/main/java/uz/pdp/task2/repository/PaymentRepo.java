package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task2.entity.Blog;
import uz.pdp.task2.entity.Payment;
import uz.pdp.task2.projection.BlogProjection;
import uz.pdp.task2.projection.PaymentProjection;

@RepositoryRestResource(path = "payment", collectionResourceRel = "list", excerptProjection = PaymentProjection.class)
public interface PaymentRepo extends JpaRepository<Payment, Long> {


}
