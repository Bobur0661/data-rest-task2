package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.Order;

public interface OrderRepo extends JpaRepository<Order, Long> {
}
