package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.projection.AddressProjection;

@RepositoryRestResource(path = "address", collectionResourceRel = "list", excerptProjection = AddressProjection.class)
public interface AddressRepo extends JpaRepository<Address, Long> {


}
