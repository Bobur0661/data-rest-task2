package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.entity.SocialLinks;
import uz.pdp.task2.projection.BrandProjection;
import uz.pdp.task2.projection.SocialLinksProjection;

@RepositoryRestResource(path = "links", collectionResourceRel = "list", excerptProjection = SocialLinksProjection.class)
public interface SocialLinksRepo extends JpaRepository<SocialLinks, Long> {


}
