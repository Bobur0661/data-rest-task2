package uz.pdp.task2.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class UserDto {


    private String name;
    private String phoneNumber;
    private Long addressId;

}
