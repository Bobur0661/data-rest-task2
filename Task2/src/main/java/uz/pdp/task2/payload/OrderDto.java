package uz.pdp.task2.payload;

import lombok.Data;

@Data
public class OrderDto {

    private Long id;
    private Long userId;
    private Long paymentId;

}
