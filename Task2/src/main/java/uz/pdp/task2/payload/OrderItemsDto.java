package uz.pdp.task2.payload;

import lombok.Data;

@Data
public class OrderItemsDto {


    private Long id;
    private double price;
    private Integer amount;
    private Long productId;
    private Long orderId;


}
