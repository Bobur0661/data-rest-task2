package uz.pdp.task2.payload;

import lombok.Data;

@Data
public class ReviewsDto {

    private Long id;
    private String description;
    private Long userId;

}
