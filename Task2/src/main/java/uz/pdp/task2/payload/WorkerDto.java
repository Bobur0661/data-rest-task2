package uz.pdp.task2.payload;

import lombok.Data;

@Data
public class WorkerDto {


    private String name;
    private String phoneNumber;
    private String position;
    private Long professionId;

}
