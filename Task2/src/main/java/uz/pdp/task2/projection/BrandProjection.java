package uz.pdp.task2.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.entity.Brand;

@Projection(types = Brand.class)
public interface BrandProjection {

    Long getId();

    String getName();

    boolean getActive();
}
