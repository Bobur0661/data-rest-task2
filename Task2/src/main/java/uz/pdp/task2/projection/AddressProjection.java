package uz.pdp.task2.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task2.entity.Address;

@Projection(types = Address.class)
public interface AddressProjection {

    Long getId();

    String getStreet();

    String getCity();
}
