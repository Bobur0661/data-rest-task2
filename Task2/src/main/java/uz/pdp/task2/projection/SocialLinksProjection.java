package uz.pdp.task2.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.entity.SocialLinks;

@Projection(types = SocialLinks.class)
public interface SocialLinksProjection {

    Long getId();

    String getName();

    String getLink();
}
