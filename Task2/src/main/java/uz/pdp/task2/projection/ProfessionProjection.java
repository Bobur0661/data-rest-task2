package uz.pdp.task2.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.entity.Profession;

@Projection(types = Profession.class)
public interface ProfessionProjection {

    Long getId();

    String getName();

    boolean getActive();
}
