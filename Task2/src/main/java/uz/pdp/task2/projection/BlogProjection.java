package uz.pdp.task2.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.entity.Blog;

@Projection(types = Blog.class)
public interface BlogProjection {

    Long getId();

    String getDescription();
}
