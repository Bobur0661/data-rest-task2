package uz.pdp.task2.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task2.entity.Blog;
import uz.pdp.task2.entity.Payment;

@Projection(types = Payment.class)
public interface PaymentProjection {

    Long getId();

    String getName();

    boolean getActive();
}
