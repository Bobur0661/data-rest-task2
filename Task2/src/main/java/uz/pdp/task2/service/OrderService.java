package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Order;
import uz.pdp.task2.entity.Payment;
import uz.pdp.task2.entity.Reviews;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.OrderDto;
import uz.pdp.task2.payload.ReviewsDto;
import uz.pdp.task2.repository.OrderRepo;
import uz.pdp.task2.repository.PaymentRepo;
import uz.pdp.task2.repository.UserRepo;

import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PaymentRepo paymentRepo;


    public ApiResponse getById(Long id) {
        Optional<Order> optionalOrder = orderRepo.findById(id);
        if (!optionalOrder.isPresent()) {
            return new ApiResponse(false, "Order not found!");
        }
        return new ApiResponse(true, "Success", optionalOrder.get());
    }


    public ApiResponse addOrUpdate(OrderDto dto) {
        Order order = new Order();
        if (dto.getId() != null) {
            order = orderRepo.getById(dto.getId());
        }
        Optional<User> optionalUser = userRepo.findById(dto.getUserId());
        if (!optionalUser.isPresent()) {
            return new ApiResponse(false, "User not found!");
        }
        Optional<Payment> optionalPayment = paymentRepo.findById(dto.getPaymentId());
        if (!optionalPayment.isPresent()) {
            return new ApiResponse(false, "Payment not found!");
        }

        order.setPayment(optionalPayment.get());
        order.setUser(optionalUser.get());
        orderRepo.save(order);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }

    public ApiResponse delete(Long id) {
        try {
            orderRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on deleteing!");
        }
    }


}
