package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.UserDto;
import uz.pdp.task2.repository.AddressRepo;
import uz.pdp.task2.repository.UserRepo;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepo userRepo;

    @Autowired
    AddressRepo addressRepo;

    public ApiResponse add(UserDto dto) {
        try {
            boolean existsByPhoneNumber = userRepo.existsByPhoneNumber(dto.getPhoneNumber());
            if (existsByPhoneNumber) {
                return new ApiResponse(false, "This phone number already exists!");
            }
            Optional<Address> optionalAddress = addressRepo.findById(dto.getAddressId());
            if (!optionalAddress.isPresent()) {
                return new ApiResponse(false, "Address not found!");
            }
            User user = new User();
            user.setUsername(dto.getName());
            user.setPhoneNumber(dto.getPhoneNumber());
            user.setAddress(optionalAddress.get());
            userRepo.save(user);
            return new ApiResponse(true, "Saved");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Saving!");
        }
    }

    public ApiResponse update(Long id, UserDto dto) {
        Optional<User> optionalUser = userRepo.findById(id);
        if (!optionalUser.isPresent())
            return new ApiResponse(false, "Client not found!");

        User user = optionalUser.get();
        boolean existsByPhoneNumberAndIdNot = userRepo.existsByPhoneNumberAndIdNot(dto.getPhoneNumber(), id);
        if (existsByPhoneNumberAndIdNot) {
            return new ApiResponse(false, "This phone number already exists!");
        }
        Optional<Address> optionalAddress = addressRepo.findById(dto.getAddressId());
        if (!optionalAddress.isPresent()) {
            return new ApiResponse(false, "Address not found!");
        }
        user.setUsername(dto.getName());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setAddress(optionalAddress.get());
        userRepo.save(user);
        return new ApiResponse(true, "Updated");
    }

    public ApiResponse delete(Long id) {
        Optional<User> optionalUser = userRepo.findById(id);
        if (!optionalUser.isPresent()) {
            return new ApiResponse(false, "Error on Deleting!");
        }
        userRepo.delete(optionalUser.get());
        return new ApiResponse(true, "Deleted!");
    }
}
