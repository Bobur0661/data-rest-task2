package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.*;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.OrderDto;
import uz.pdp.task2.payload.OrderItemsDto;
import uz.pdp.task2.repository.OrderItemsRepo;
import uz.pdp.task2.repository.OrderRepo;
import uz.pdp.task2.repository.ProductRepo;

import java.util.Optional;

@Service
public class OrderItemsService {


    @Autowired
    OrderItemsRepo orderItemsRepo;

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    ProductRepo productRepo;


    public ApiResponse getById(Long id) {
        Optional<OrderItems> optionalOrderItems = orderItemsRepo.findById(id);
        return optionalOrderItems.map(orderItems -> new ApiResponse(true, "Success", orderItems))
                .orElseGet(() -> new ApiResponse(false, "OrderItems not found!"));
    }


    public ApiResponse addOrUpdate(OrderItemsDto dto) {
        OrderItems orderItems = new OrderItems();
        if (dto.getId() != null) {
            orderItems = orderItemsRepo.getById(dto.getId());
        }
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (!optionalProduct.isPresent()) {
            return new ApiResponse(false, "Product not found!");
        }
        Optional<Order> optionalOrder = orderRepo.findById(dto.getOrderId());
        if (!optionalOrder.isPresent()) {
            return new ApiResponse(false, "Order not found!");
        }
        orderItems.setAmount(dto.getAmount());
        orderItems.setPrice(dto.getPrice());
        orderItems.setProduct(optionalProduct.get());
        orderItems.setOrder(optionalOrder.get());
        orderItemsRepo.save(orderItems);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }

    public ApiResponse delete(Long id) {
        try {
            orderItemsRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on deleteing!");
        }
    }


}
