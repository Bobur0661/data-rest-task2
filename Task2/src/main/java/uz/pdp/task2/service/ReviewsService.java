package uz.pdp.task2.service;

import com.sun.xml.internal.ws.api.pipe.helper.AbstractPipeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Reviews;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.ReviewsDto;
import uz.pdp.task2.repository.ReviewsRepo;
import uz.pdp.task2.repository.UserRepo;

import java.util.Optional;

@Service
public class ReviewsService {

    @Autowired
    ReviewsRepo reviewsRepo;

    @Autowired
    UserRepo userRepo;


    public ApiResponse getById(Long id) {
        Optional<Reviews> reviewsOptional = reviewsRepo.findById(id);
        if (!reviewsOptional.isPresent()) {
            return new ApiResponse(false, "Review not found!");
        }
        return new ApiResponse(true, "Success", reviewsOptional.get());
    }


    public ApiResponse addOrUpdate(ReviewsDto dto) {
        Reviews reviews = new Reviews();
        if (dto.getId() != null) {
            reviews = reviewsRepo.getById(dto.getId());
        }
        Optional<User> optionalUser = userRepo.findById(dto.getUserId());
        if (!optionalUser.isPresent()) {
            return new ApiResponse(false, "User not found!");
        }
        reviews.setDescription(dto.getDescription());
        reviews.setUser(optionalUser.get());
        reviewsRepo.save(reviews);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }

    public ApiResponse delete(Long id) {
        try {
            reviewsRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on deleteing!");
        }
    }
}
