package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Address;
import uz.pdp.task2.entity.Profession;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.entity.Worker;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.WorkerDto;
import uz.pdp.task2.repository.ProfessionRepo;
import uz.pdp.task2.repository.WorkerRepo;

import java.util.Optional;

@Service
public class WorkerService {

    @Autowired
    WorkerRepo workerRepo;

    @Autowired
    ProfessionRepo professionRepo;

    public ApiResponse add(WorkerDto dto) {
        try {
            boolean existsByPhoneNumber = workerRepo.existsByPhoneNumber(dto.getPhoneNumber());
            if (existsByPhoneNumber) {
                return new ApiResponse(false, "This phone number already exists!");
            }
            Optional<Profession> optionalProfession = professionRepo.findById(dto.getProfessionId());
            if (!optionalProfession.isPresent()) {
                return new ApiResponse(false, "Profession not found!");
            }
            Worker worker = new Worker();
            worker.setName(dto.getName());
            worker.setPhoneNumber(dto.getPhoneNumber());
            worker.setPosition(dto.getPosition());
            worker.setProfession(optionalProfession.get());
            workerRepo.save(worker);
            return new ApiResponse(true, "Saved");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Saving!");
        }
    }

    public ApiResponse update(Long id, WorkerDto dto) {
        Optional<Worker> workerOptional = workerRepo.findById(id);
        if (!workerOptional.isPresent())
            return new ApiResponse(false, "Worker not found!");

        boolean existsByPhoneNumberAndIdNot = workerRepo.existsByPhoneNumberAndIdNot(dto.getPhoneNumber(), id);
        if (existsByPhoneNumberAndIdNot) {
            return new ApiResponse(false, "This phone number already exists!");
        }
        Optional<Profession> optionalProfession = professionRepo.findById(dto.getProfessionId());
        if (!optionalProfession.isPresent()) {
            return new ApiResponse(false, "Profession not found!");
        }
        Worker worker = workerOptional.get();
        worker.setName(dto.getName());
        worker.setPhoneNumber(dto.getPhoneNumber());
        worker.setPosition(dto.getPosition());
        worker.setProfession(optionalProfession.get());
        workerRepo.save(worker);
        return new ApiResponse(true, "Updated");
    }

    public ApiResponse delete(Long id) {
        Optional<Worker> optionalWorker = workerRepo.findById(id);
        if (!optionalWorker.isPresent()) {
            return new ApiResponse(false, "Error on Deleting!");
        }
        workerRepo.delete(optionalWorker.get());
        return new ApiResponse(true, "Deleted!");
    }
}
