package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.entity.Category;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.CategoryDto;
import uz.pdp.task2.repository.BrandRepo;
import uz.pdp.task2.repository.CategoryRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {


    @Autowired
    CategoryRepo categoryRepo;


    @Autowired
    BrandRepo brandRepo;

    public ApiResponse getAll() {
        try {
            List<Category> all = categoryRepo.findAll();
            return new ApiResponse(true, "Success", all);
        } catch (Exception e) {
            return new ApiResponse(false, "Cannot Find");
        }
    }


    public ApiResponse getById(Long id) {
        Optional<Category> optionalCategory = categoryRepo.findById(id);
        return optionalCategory.map(user -> new ApiResponse(true, "Success", optionalCategory))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    public ApiResponse addOrUpdate(CategoryDto dto) {
        boolean existsByName = categoryRepo.existsByName(dto.getName());

        // Kiritilgan brand lar bor mi yoki qo'qmi shuni tekshirvoladi!
        for (Brand brand : dto.getBrands()) {
            Optional<Brand> optionalBrand = brandRepo.findById(brand.getId());
            if (!optionalBrand.isPresent())
                return new ApiResponse(false, "There is no such brand " + brand.getName());
        }

        // Agar dto.getId teng bo'lsa null ga unda save qilamiz
        if (dto.getId() == null) {
            if (existsByName) {
                return new ApiResponse(false, "The category already exists!");
            }
            Category category = new Category();
            // Dto ning parentId si null ga teng bo'lmasa unda ota categoriya mavjud bo'ladi.
            if (dto.getParentCategoryId() != null) {
                Optional<Category> parentCategoryId = categoryRepo.findById(dto.getParentCategoryId());
                if (!parentCategoryId.isPresent())
                    return new ApiResponse(false, "Parent Category not found!");
                category.setParentCategoryId(parentCategoryId.get());
            }
            category.setName(dto.getName());
            category.setBrands(dto.getBrands());
            categoryRepo.save(category);
            return new ApiResponse(true, "Saved");
        } else { // Update taraf
            Optional<Category> optionalCategory = categoryRepo.findById(dto.getId());
            Category category = optionalCategory.get();
            if (!optionalCategory.isPresent()) {
                return new ApiResponse(false, "Category not found!");
            } else if (optionalCategory.get().getName().equals(dto.getName())) {
                if (dto.getParentCategoryId() != null) {
                    Optional<Category> parentCategoryId = categoryRepo.findById(dto.getParentCategoryId());
                    if (!parentCategoryId.isPresent())
                        return new ApiResponse(false, "Parent Category not found!");
                    category.setParentCategoryId(parentCategoryId.get());
                }
                category.setBrands(dto.getBrands());
                categoryRepo.save(category);
                return new ApiResponse(true, "Edited");
            } else {
                if (dto.getParentCategoryId() != null) {
                    Optional<Category> parentCategoryId = categoryRepo.findById(dto.getParentCategoryId());
                    if (!parentCategoryId.isPresent())
                        return new ApiResponse(false, "Parent Category not found!");
                    category.setParentCategoryId(parentCategoryId.get());
                }
                boolean existsByNameAndIdNot = categoryRepo.existsByNameAndIdNot(dto.getName(), dto.getId());
                if (existsByNameAndIdNot) {
                    return new ApiResponse(false, "Category Already exists");
                }
                category.setName(dto.getName());
                category.setBrands(dto.getBrands());
                categoryRepo.save(category);
                return new ApiResponse(true, "Edited");
            }
        }
    }


    public ApiResponse delete(Long id) {
        try {
            categoryRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
