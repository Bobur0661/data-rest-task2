package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Brand;
import uz.pdp.task2.entity.Product;
import uz.pdp.task2.entity.Reviews;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.ProductDto;
import uz.pdp.task2.payload.ReviewsDto;
import uz.pdp.task2.repository.BrandRepo;
import uz.pdp.task2.repository.ProductRepo;

import java.util.Optional;

@Service
public class ProductService {


    @Autowired
    ProductRepo productRepo;

    @Autowired
    BrandRepo brandRepo;

    public ApiResponse getById(Long id) {
        Optional<Product> optionalProduct = productRepo.findById(id);
        if (!optionalProduct.isPresent()) {
            return new ApiResponse(false, "Product not found!");
        }
        return new ApiResponse(true, "Success", optionalProduct.get());
    }


    public ApiResponse addOrUpdate(ProductDto dto) {
        boolean existsByName = productRepo.existsByName(dto.getName());
        Product product = new Product();
        if (dto.getId() == null) {
            if (existsByName) {
                return new ApiResponse(false, "The product already exists");
            }
            Optional<Brand> optionalBrand = brandRepo.findById(dto.getBrandId());
            if (!optionalBrand.isPresent()) {
                return new ApiResponse(false, "Brand not found!");
            }
            product.setPrice(dto.getPrice());
            product.setName(dto.getName());
            product.setBrand(optionalBrand.get());
            productRepo.save(product);
            return new ApiResponse(true, "Saved");
        } else {
            Optional<Product> optionalProduct = productRepo.findById(dto.getId());
            if (!optionalProduct.isPresent()) {
                return new ApiResponse(false, "Product not found!");
            }
            boolean existsByNameAndIdNot = productRepo.existsByNameAndIdNot(dto.getName(), dto.getId());
            if (existsByNameAndIdNot) {
                return new ApiResponse(false, "The product already exists");
            }
            Optional<Brand> optionalBrand = brandRepo.findById(dto.getBrandId());
            if (!optionalBrand.isPresent()) {
                return new ApiResponse(false, "Brand not found!");
            }
            Product product1 = optionalProduct.get();
            product1.setPrice(dto.getPrice());
            product1.setName(dto.getName());
            product1.setBrand(optionalBrand.get());
            productRepo.save(product1);
            return new ApiResponse(true, "Updated");
        }
    }

    public ApiResponse delete(Long id) {
        try {
            productRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on deleteing!");
        }
    }


}
