package uz.pdp.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.task2.entity.template.AbsNameIdActive;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Worker extends AbsNameIdActive {


    @Column(nullable = false)
    private String position;

    @Column(unique = true, nullable = false)
    private String phoneNumber;

    @OneToOne
    private Profession profession;

}
