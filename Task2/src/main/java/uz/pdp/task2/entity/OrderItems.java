package uz.pdp.task2.entity;

import lombok.*;
import uz.pdp.task2.entity.template.AbsId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrderItems extends AbsId {


    @ManyToOne
    private Product product;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private Integer amount;

    @ManyToOne
    private Order order;
}
