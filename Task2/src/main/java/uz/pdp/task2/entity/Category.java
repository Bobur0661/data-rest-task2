package uz.pdp.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.task2.entity.template.AbsNameIdActive;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Category extends AbsNameIdActive {

    @ManyToOne // Ko'plab bo'la kategoriyalarni bitta otasi bo'ladi, yoki otasi bo'lmashligi ham mumkin:
    private Category parentCategoryId; // null, 1, 1, 20

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToMany
    private List<Brand> brands;

}
