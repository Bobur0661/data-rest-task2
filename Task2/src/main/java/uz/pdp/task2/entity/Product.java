package uz.pdp.task2.entity;

import lombok.*;
import uz.pdp.task2.entity.template.AbsId;
import uz.pdp.task2.entity.template.AbsNameIdActive;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsId {


    @Column(unique = true, nullable = false)
    private String name;

    private boolean active=true;

    @Column(nullable = false)
    private double price;

    @ManyToOne
    private Brand brand;

}
