package uz.pdp.task2.entity;

import lombok.*;
import uz.pdp.task2.entity.template.AbsNameIdActive;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Payment extends AbsNameIdActive {

}
