package uz.pdp.task2.entity;

import lombok.*;
import uz.pdp.task2.entity.template.AbsId;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "orders")
public class Order extends AbsId {


    @ManyToOne
    private User user;

    @ManyToOne
    private Payment payment;

}
