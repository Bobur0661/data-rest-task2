package uz.pdp.task2.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.task2.entity.template.AbsNameIdActive;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Profession extends AbsNameIdActive {

}
