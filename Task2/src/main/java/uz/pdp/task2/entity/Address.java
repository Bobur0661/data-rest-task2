package uz.pdp.task2.entity;

import lombok.*;
import uz.pdp.task2.entity.template.AbsId;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Address extends AbsId {

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String city;

}
