package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.entity.Worker;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.WorkerDto;
import uz.pdp.task2.repository.WorkerRepo;
import uz.pdp.task2.service.WorkerService;

import java.util.List;

@RestController
@RequestMapping("/api/worker")
public class WorkerController {


    @Autowired
    WorkerService workerService;

    @Autowired
    WorkerRepo workerRepo;


    @GetMapping
    public List<Worker> get() {
        return workerRepo.findAll();
    }


    @PostMapping
    public HttpEntity<?> add(@RequestBody WorkerDto dto) {
        ApiResponse apiResponse = workerService.add(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @PutMapping("/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody WorkerDto dto) {
        return workerService.update(id, dto);
    }


    @DeleteMapping("{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return workerService.delete(id);
    }

}
