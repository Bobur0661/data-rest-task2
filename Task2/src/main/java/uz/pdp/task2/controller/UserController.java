package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.UserDto;
import uz.pdp.task2.repository.UserRepo;
import uz.pdp.task2.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {


    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;


    @GetMapping
    public List<User> get() {
        return userRepo.findAll();
    }


    @PostMapping
    public HttpEntity<?> add(@RequestBody UserDto dto) {
        ApiResponse apiResponse = userService.add(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @PutMapping("/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody UserDto dto) {
        return userService.update(id, dto);
    }


    @DeleteMapping("/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return userService.delete(id);
    }

}
