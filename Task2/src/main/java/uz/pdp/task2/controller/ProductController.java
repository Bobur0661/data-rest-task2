package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.entity.Product;
import uz.pdp.task2.entity.Reviews;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.ProductDto;
import uz.pdp.task2.payload.ReviewsDto;
import uz.pdp.task2.repository.ProductRepo;
import uz.pdp.task2.repository.ReviewsRepo;
import uz.pdp.task2.service.ProductService;
import uz.pdp.task2.service.ReviewsService;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductRepo productRepo;

    @Autowired
    ProductService productService;

    @GetMapping
    public List<Product> getAll() {
        return productRepo.findAll();
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = productService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addOrUpdate(@RequestBody ProductDto dto) {
        ApiResponse apiResponse = productService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved") ? 201
                : apiResponse.getMessage().equals("Updated") ? 202 : 409).body(apiResponse);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = productService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

}
