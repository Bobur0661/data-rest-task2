package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.CategoryDto;
import uz.pdp.task2.service.CategoryService;

@RestController
@RequestMapping("/api/category")
public class CategoryController {


    @Autowired
    CategoryService categoryService;


    @GetMapping
    public HttpEntity<?> getAll() {
        ApiResponse apiResponse = categoryService.getAll();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = categoryService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }


    @PostMapping
    public HttpEntity<?> add(@RequestBody CategoryDto dto) {
        ApiResponse apiResponse = categoryService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201
                : 409).body(apiResponse);
    }


    @PutMapping("/update")
    public HttpEntity<?> update(@RequestBody CategoryDto dto) {
        ApiResponse apiResponse = categoryService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Updated") ? 202
                : 409).body(apiResponse);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = categoryService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
