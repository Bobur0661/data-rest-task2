package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.entity.OrderItems;
import uz.pdp.task2.entity.Product;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.OrderItemsDto;
import uz.pdp.task2.payload.ProductDto;
import uz.pdp.task2.repository.OrderItemsRepo;
import uz.pdp.task2.repository.ProductRepo;
import uz.pdp.task2.service.OrderItemsService;
import uz.pdp.task2.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/api/orderItems")
public class OrderItemsController {

    @Autowired
    OrderItemsRepo orderItemsRepo;

    @Autowired
    OrderItemsService orderItemsService;

    @GetMapping
    public List<OrderItems> getAll() {
        return orderItemsRepo.findAll();
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = orderItemsService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addOrUpdate(@RequestBody OrderItemsDto dto) {
        ApiResponse apiResponse = orderItemsService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved") ? 201
                : apiResponse.getMessage().equals("Updated") ? 202 : 409).body(apiResponse);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = orderItemsService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

}
